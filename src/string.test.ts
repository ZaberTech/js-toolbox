import { makeString } from './string';

describe('the makeString method', () => {
  test('returns a string representation of the input', () => {
    expect(makeString('hello')).toBe('hello');
    expect(makeString(123)).toBe('123');
    expect(makeString([1, null, ['hi', 3]])).toBe('[1, null, [hi, 3]]');
    expect(makeString({ a: 1, b: 'blah' })).toBe('{"a":1,"b":"blah"}');
    expect(makeString(new Error('oops'))).toBe('Error: oops');
  });
});
