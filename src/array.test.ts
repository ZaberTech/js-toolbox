import { ensureArray } from './array';

describe('ensureArray', () => {
  test('returns array of one for a non-array value', () => {
    expect(ensureArray(3)).toEqual([3]);
  });
  test('returns passed value for an array', () => {
    const array = [3];
    expect(ensureArray(array)).toBe(array);
  });
  test('returns passed value for null or undefined', () => {
    expect(ensureArray<number>(undefined)).toBeUndefined();
    expect(ensureArray<number>(null)).toBeNull();
  });
});
