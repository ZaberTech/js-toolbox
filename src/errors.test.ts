import { ensureError } from './errors';

describe('ensureError', () => {
  test('ensures that returned type is error', () => {
    const error = ensureError('weird string from library');
    expect(error).toBeInstanceOf(Error);
    expect(error.message).toBe('weird string from library');
  });
  test('does not change passed errors', () => {
    const error = new Error('actual error');
    expect(ensureError(error)).toBe(error);
  });
});
