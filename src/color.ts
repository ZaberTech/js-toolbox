// Miscellaneous color conversion functions.

export interface ColorRGB { r: number; g: number; b: number }
export interface ColorRGBA extends ColorRGB { a: number }
export interface ColorHSV { h: number; s: number; v: number }

/**
 * Convert an HTML/CSS color string to an RGB color with components in the range 0-255.
 * @param hex Color to convert: 3 or 6 hex digits with optional leading '#'.
 * @returns Color as non-normalized RGB components.
 */
export function stringToRGB(hex: string): { r: number; g: number; b: number } {
  if (hex.length < 6) {
    const result = /^#?([a-f\d])([a-f\d])([a-f\d])$/i.exec(hex);
    return {
      r: parseInt(result![1], 16) * 17,
      g: parseInt(result![2], 16) * 17,
      b: parseInt(result![3], 16) * 17,
    };
  } else {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return {
      r: parseInt(result![1], 16),
      g: parseInt(result![2], 16),
      b: parseInt(result![3], 16),
    };
  }
}


/**
 * Convert an RGB color with components in the 0-255 range to an HTML/CSS color string.
 * Clamps values and adds a leading '#'.
 */
export function stringFromRGB(color: ColorRGB): string {
  const r = Math.max(0, Math.min(255, color.r));
  const g = Math.max(0, Math.min(255, color.g));
  const b = Math.max(0, Math.min(255, color.b));
  return `#${(r << 16 | g << 8 | b).toString(16).padStart(6, '0')}`;
}


/**
 * Convert an RGB color with components in the 0-255 range to the 0-1 range.
 * Does not do range checking.
 * @param color Color to convert.
 * @returns Converted color.
 */
export function normalizeRGB(color: ColorRGB): ColorRGB {
  return {
    r: color.r / 255,
    g: color.g / 255,
    b: color.b / 255,
  };
}


/**
 * Convert an RGB color with components in the 0-1 range to the 0-255 range.
 * Does not do range checking.
 * @param color Color to convert.
 * @returns Converted color.
 */
export function denormalizeRGB(color: ColorRGB): ColorRGB {
  return {
    r: Math.floor(color.r * 255),
    g: Math.floor(color.g * 255),
    b: Math.floor(color.b * 255),
  };
}


/**
 * Convert an RGBA color with components in the 0-255 range to the 0-1 range.
 * Does not do range checking.
 * @param color Color to convert.
 * @returns Converted color.
 */
export function normalizeRGBA(color: ColorRGBA): ColorRGBA {
  return {
    r: color.r / 255,
    g: color.g / 255,
    b: color.b / 255,
    a: color.a / 255,
  };
}


/**
 * Convert an RGBA color with components in the 0-1 range to the 0-255 range.
 * Does not do range checking.
 * @param color Color to convert.
 * @returns Converted color.
 */
export function denormalizeRGBA(color: ColorRGBA): ColorRGBA {
  return {
    r: Math.floor(color.r * 255),
    g: Math.floor(color.g * 255),
    b: Math.floor(color.b * 255),
    a: Math.floor(color.a * 255),
  };
}


/** Packs RGB color values (0 to 255 range) into a single integer in 0x00RRGGBB format. */
export function packRGB(color: ColorRGB): number {
  return ((color.r & 0xFF) << 16) | ((color.g & 0xFF) << 8) | (color.b & 0xFF);
}


/** Packs RGB color values (0 to 255 range) into a single integer in 0x00RRGGBB format. */
export function packRGBFromComponents(r: number, g: number, b: number): number {
  return ((r & 0xFF) << 16) | ((g & 0xFF) << 8) | (b & 0xFF);
}


/** Packs RGB color values (0 to 255 range) into a single integer in 0x00RRGGBB format. */
export function packRGBFromArray(components: [number, number, number]): number {
  return packRGBFromComponents(...components);
}


/** Unpacks an integer in 0x00RRGGBB format into a ColorRGB object with components in the range 0-255. */
export function unpackRGB(color: number): ColorRGB {
  const r = (color >> 16) & 0xFF;
  const g = (color >> 8) & 0xFF;
  const b = color & 0xFF;
  return { r, g, b };
}


/** Unpacks an integer in 0x00RRGGBB format into three separate numbers (0-255). */
export function unpackRGBToArray(color: number): [number, number, number] {
  return [(color >> 16) & 0xFF, (color >> 8) & 0xFF, color & 0xFF];
}
