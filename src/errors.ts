import { makeString } from './string';

const jsLanguageErrors = [TypeError, EvalError, RangeError, ReferenceError, SyntaxError];
/**
 * Rethrows an error if the type matches set of predefined JS language errors (e.g. TypeError).
 */
export function throwIfJsError(error: unknown): void {
  if (jsLanguageErrors.some(ErrorType => error instanceof ErrorType)) {
    throw error;
  }
}

/** Re-throws an error if it originated from javascript or is not an instance of Error */
export function throwUnexpectedError(error: unknown): asserts error is Error {
  if (!(error instanceof Error)) {
    throw new Error(`Caught exception data: ${String(error)}`);
  }
  throwIfJsError(error);
}

export function ensureError(err: unknown): Error {
  if (err instanceof Error) {
    return err;
  }
  return new Error(makeString(err));
}
