import { toBeDeepCloseTo } from 'jest-matcher-deep-close-to';

import {
  denormalizeRGB,
  denormalizeRGBA,
  normalizeRGB,
  normalizeRGBA,
  packRGB,
  packRGBFromArray,
  packRGBFromComponents,
  stringFromRGB,
  stringToRGB,
  unpackRGB,
  unpackRGBToArray
} from './color';

expect.extend({ toBeDeepCloseTo });


describe('Conversion from strings', () => {
  it('converts 3-digit hex strings', () => {
    expect(stringToRGB('#000')).toEqual({ r: 0, g: 0, b: 0 });
    expect(stringToRGB('#18E')).toEqual({ r: 0x11, g: 0x88, b: 0xEE });
    expect(stringToRGB('#FFF')).toEqual({ r: 0xFF, g: 0xFF, b: 0xFF });
  });

  it('converts 6-digit hex strings', () => {
    expect(stringToRGB('#000000')).toEqual({ r: 0, g: 0, b: 0 });
    expect(stringToRGB('#123456')).toEqual({ r: 0x12, g: 0x34, b: 0x56 });
    expect(stringToRGB('#FFFFFF')).toEqual({ r: 0xFF, g: 0xFF, b: 0xFF });
  });

  it('handles missing number signs', () => {
    expect(stringToRGB('123')).toEqual({ r: 0x11, g: 0x22, b: 0x33 });
    expect(stringToRGB('123456')).toEqual({ r: 0x12, g: 0x34, b: 0x56 });
  });

  it('handles lowercase', () => {
    expect(stringToRGB('abc')).toEqual({ r: 0xAA, g: 0xBB, b: 0xCC });
    expect(stringToRGB('abcdef')).toEqual({ r: 0xAB, g: 0xCD, b: 0xEF });
  });
});


describe('Conversion to strings', () => {
  it('produces expected hex strings', () => {
    expect(stringFromRGB({ r: 0, g: 0, b: 0 })).toEqual('#000000');
    expect(stringFromRGB({ r: 0x12, g: 0x34, b: 0x56 })).toEqual('#123456');
    expect(stringFromRGB({ r: 0xFF, g: 0xFF, b: 0xFF })).toEqual('#ffffff');
  });
});


describe('Normalization', () => {
  it('produces expected RGB values', () => {
    expect(normalizeRGB({ r: 0, g: 0, b: 0 })).toEqual({ r: 0, g: 0, b: 0 });
    expect(normalizeRGB({ r: 255, g: 255, b: 255 })).toEqual({ r: 1, g: 1, b: 1 });
    expect(normalizeRGB({ r: 127, g: 127, b: 127 })).toBeDeepCloseTo({ r: 0.5, g: 0.5, b: 0.5 });
  });

  it('produces expected RGBA values', () => {
    expect(normalizeRGBA({ r: 0, g: 0, b: 0, a: 0 })).toEqual({ r: 0, g: 0, b: 0, a: 0 });
    expect(normalizeRGBA({ r: 255, g: 255, b: 255, a: 255 })).toEqual({ r: 1, g: 1, b: 1, a: 1 });
    expect(normalizeRGBA({ r: 127, g: 127, b: 127, a: 127 })).toBeDeepCloseTo({ r: 0.5, g: 0.5, b: 0.5, a: 0.5 });
  });
});


describe('Denormalization', () => {
  it('produces expected RGB values', () => {
    expect(denormalizeRGB({ r: 0, g: 0, b: 0 })).toEqual({ r: 0, g: 0, b: 0 });
    expect(denormalizeRGB({ r: 1, g: 1, b: 1 })).toEqual({ r: 255, g: 255, b: 255 });
    expect(denormalizeRGB({ r: 0.5, g: 0.5, b: 0.5 })).toEqual({ r: 127, g: 127, b: 127 });
  });

  it('produces expected RGBA values', () => {
    expect(denormalizeRGBA({ r: 0, g: 0, b: 0, a: 0 })).toEqual({ r: 0, g: 0, b: 0, a: 0 });
    expect(denormalizeRGBA({ r: 1, g: 1, b: 1, a: 1 })).toEqual({ r: 255, g: 255, b: 255, a: 255 });
    expect(denormalizeRGBA({ r: 0.5, g: 0.5, b: 0.5, a: 0.5 })).toEqual({ r: 127, g: 127, b: 127, a: 127 });
  });
});


describe('Packing', () => {
  it('packs from RGB struct', () => {
    expect(packRGB({ r: 0, g: 0, b: 0 })).toEqual(0x000000);
    expect(packRGB({ r: 0x12, g: 0x34, b: 0x56 })).toEqual(0x123456);
    expect(packRGB({ r: 255, g: 255, b: 255 })).toEqual(0xFFFFFF);
  });

  it('packs from component values', () => {
    expect(packRGBFromComponents(0, 0, 0)).toEqual(0x000000);
    expect(packRGBFromComponents(0x12, 0x34, 0x56)).toEqual(0x123456);
    expect(packRGBFromComponents(255, 255, 255)).toEqual(0xFFFFFF);
  });

  it('packs from arrays', () => {
    expect(packRGBFromArray([0, 0, 0])).toEqual(0x000000);
    expect(packRGBFromArray([0x12, 0x34, 0x56])).toEqual(0x123456);
    expect(packRGBFromArray([255, 255, 255])).toEqual(0xFFFFFF);
  });
});


describe('Unpacking', () => {
  it('unpacks to RGB struct', () => {
    expect(unpackRGB(0x000000)).toEqual({ r: 0, g: 0, b: 0 });
    expect(unpackRGB(0x123456)).toEqual({ r: 0x12, g: 0x34, b: 0x56 });
    expect(unpackRGB(0xFFFFFF)).toEqual({ r: 255, g: 255, b: 255 });
  });

  it('unpacks to array', () => {
    expect(unpackRGBToArray(0x000000)).toEqual([0, 0, 0]);
    expect(unpackRGBToArray(0x123456)).toEqual([0x12, 0x34, 0x56]);
    expect(unpackRGBToArray(0xFFFFFF)).toEqual([255, 255, 255]);
  });
});
