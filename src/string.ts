/** Takes something of an unknown type and transforms it into the most helpful string representation available */
export function makeString(something: unknown): string {
  if (Array.isArray(something)) {
    return `[${something.map(makeString).join(', ')}]`;
  }
  const stringed = String(something);
  if (stringed === '[object Object]') {
    return JSON.stringify(something);
  }
  return stringed;
}
