import { ensureError } from '../errors';
import { realTimers } from './timers';

export async function waitUntil(condition: () => boolean): Promise<void> {
  await new Promise<void>(resolve => {
    const check = () => {
      if (condition()) {
        resolve();
      } else {
        realTimers.setTimeout(check);
      }
    };

    check();
  });
}
export async function waitUntilPass<T>(callback: () => T, timeout: number = 500): Promise<T> {
  const startTS = realTimers.timestamp();
  let retry = true;
  return new Promise<T>((resolve, reject) => {
    const check = () => {
      try {
        const value = callback();
        resolve(value);
      } catch (err) {
        if (!retry) {
          return reject(ensureError(err));
        }
        const isLate = (realTimers.timestamp() - startTS) > timeout;
        if (isLate) {
          retry = false;
        }
        realTimers.setTimeout(check);
      }
    };

    check();
  });
}

export async function waitTick(ticks: number = 1): Promise<void> {
  let ticksLeft = ticks;
  await waitUntil(() => ticksLeft-- <= 0);
}
