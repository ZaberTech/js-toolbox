// This file contains test setup utilities. It needs to be imported manually.
import './timers';

export let consoleErrorSpy: jest.SpyInstance;

beforeEach(() => {
  consoleErrorSpy = jest.spyOn(global.console, 'error');
});

afterEach(() => {
  const firstCall = consoleErrorSpy.mock.calls[0];
  consoleErrorSpy.mockRestore();
  if (firstCall != null) {
    throw new Error(`Error was logged during test: ${firstCall.join(', ')}`);
  }
});
