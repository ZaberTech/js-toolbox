// captures the real timers and now function before jest mocks them

const timestamp = (() => {
  // eslint-disable-next-line @typescript-eslint/no-require-imports
  const performanceObj = typeof performance !== 'undefined' ? performance : require('perf_hooks').performance;
  return performanceObj.now.bind(performanceObj) as () => number;
})();

export const realTimers = {
  timestamp,
  setTimeout,
  clearTimeout,
  setInterval,
  clearInterval,
};
