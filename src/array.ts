import { Nullable } from './types';

export function ensureArray<T>(maybeArray: T | T[]): T[];
export function ensureArray<T>(maybeArray: Nullable<T | T[]>): Nullable<T | T[]>;
/**
 * Returns an array of one element if the passed value is defined and not an array.
 */
export function ensureArray<T>(maybeArray: Nullable<T | T[]>): Nullable<T[]> {
  if (maybeArray == null) {
    return maybeArray as Nullable<T[]>;
  }
  if (Array.isArray(maybeArray)) {
    return maybeArray;
  } else {
    return [maybeArray];
  }
}
