
export function tryAccess<T, K extends string | number>(map: Record<K, T>, key: K | null | undefined): T | undefined {
  return map[key!];
}

/**
 * Helps to create a dictionary with the supplied keys and creator
 * @param keys The keys the new dictionary should have
 * @param creator A function to create the value at each key
 * @returns The new dictionary
 */
export function buildDictionary<T, K extends string | number>(keys: K[], creator: (key: K) => T): Record<K, T> {
  return Object.fromEntries(keys.map(key => [key, creator(key)])) as Record<K, T>;
}
