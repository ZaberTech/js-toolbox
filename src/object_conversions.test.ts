/* eslint-disable camelcase */
import { convertForApi, convertFromApi, convert } from './object_conversions';

const TEST_DATA = {
  carColor1: 'red',
  CarColor2: 'red',
  car_color_3: 'red',
  CAR_COLOR_4: 'red',
  Car_Color_5: 'red',
};

describe('convertForApi', () => {
  test('converts keys to snake case', () => {
    const converted = convertForApi(TEST_DATA);

    expect(converted).toStrictEqual({
      car_color_1: 'red',
      car_color_2: 'red',
      car_color_3: 'red',
      car_color_4: 'red',
      car_color_5: 'red',
    });
  });
});

describe('convertFromApi', () => {
  test('converts keys to snake_case', () => {
    const converted = convertFromApi(TEST_DATA);

    expect(converted).toStrictEqual({
      carColor1: 'red',
      carColor2: 'red',
      carColor3: 'red',
      carColor4: 'red',
      carColor5: 'red',
    });
  });
});

describe('convert', () => {
  test('converts all keys in object using provided functions', () => {
    const converted = convert({
      carColor: 'red',
      nullItem: null,
      moreItems: [{
        tastyFood: ['pizza']
      }, {
        healthyFood: ['brocoli']
      }],
      NestedItem: {
        moreNesting: {
          api_case: 3,
        },
      },
    }, key => `_${key}_`);

    expect(converted).toStrictEqual({
      _carColor_: 'red',
      _nullItem_: null,
      _moreItems_: [{
        _tastyFood_: ['pizza']
      }, {
        _healthyFood_: ['brocoli']
      }],
      _NestedItem_: {
        _moreNesting_: {
          _api_case_: 3,
        },
      },
    });
  });
});
