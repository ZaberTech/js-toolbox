import _ from 'lodash';

import type { AnyCtor } from './types';

export class MyNumber {
  static isFinite(n: unknown): n is number {
    return Number.isFinite(n);
  }
}

export function notNil<T>(item: (T | undefined | null)): item is T {
  return item != null;
}

export function castAny<T>(value: unknown, NewT: AnyCtor<T>): T | undefined {
  return value instanceof NewT ? value as T : undefined;
}
export function cast<T, S extends T>(value: S | null | undefined, NewT: AnyCtor<T>): T | undefined {
  return castAny(value, NewT);
}

/**
 * A more type safe version of `_.pick()`. But it is more limited because of it.
 * The `props` arguments will be typechecked to only allow for properties that actually exist
 * on the `object` and the returned object will have the correct types as well.
 * But this function's typechecking setup does not allow for the advanced functionality
 * provided by `_.pick()`
 * @param object The object to pick from
 * @param props The keys to pick out of the object. Lodash style paths are not supported.
 * @returns A new object with only the picked properties from the input object
 */
export const pick = <T, U extends keyof T>(object: T, ...props: U[]) => _.pick(object, ...props) as Pick<T, U>;

/**
 * A more type safe version of `_.omit()`.
 * The `props` arguments will be typechecked to only allow for properties that actually exist
 * on the `object` and the returned object will have the correct types as well.
 * @param object The object to omit from
 * @param props The keys to omit from the object. Lodash style paths are not supported.
 * @returns A new object with only the omitted properties from the input object
 */
export const omit = <T extends object, U extends keyof T>(object: T, ...props: U[]) =>
  _.omit(object, ...props) as Omit<T, U>;
