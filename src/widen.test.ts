/* eslint-disable @typescript-eslint/ban-ts-comment */
import type { Widen, DeepWiden } from './widen';

interface ConfigSerial {
  id: number;
  serialPort: string;
}

interface ConfigTcp {
  id: number;
  hostname: string;
}

type Config = ConfigSerial | ConfigTcp;

const CONFIG = { id: 1, serialPort: 'COM1' } as Config;

describe('Widen', () => {
  test('makes distinct properties accessible but optional', () => {
    // @ts-expect-error
    expect(CONFIG.serialPort).toBe('COM1');
    const wideConfig: Widen<Config> = CONFIG;
    expect(wideConfig.serialPort).toBe('COM1');

    // @ts-expect-error
    expect(wideConfig.serialPort.slice(1)).toBe('OM1');
    expect(wideConfig.serialPort?.slice(1)).toBe('OM1');

    expect(wideConfig.hostname).toBeUndefined();
    expect(wideConfig.hostname?.slice(1)).toBeUndefined();
  });

  test('keeps common properties accessible', () => {
    const wideConfig: Widen<Config> = CONFIG;
    expect(wideConfig.id + 1).toBe(2);
  });
});

interface Info {
  name: string;
}

interface DeviceConfig {
  info: Info;
  config: Config;
}

interface PeripheralConfig {
  info: Info;
  config: {
    speed: number;
  };
}

type NestedConfig = DeviceConfig | PeripheralConfig;

const NESTED_CONFIG = { info: { name: 'XLS' }, config: CONFIG } as NestedConfig;

describe('DeepWiden', () => {
  test('makes distinct properties accessible but optional', () => {
    // @ts-expect-error
    expect(NESTED_CONFIG.config.serialPort).toBe('COM1');
    const wideConfig: DeepWiden<NestedConfig> = NESTED_CONFIG;
    expect(wideConfig.config.serialPort).toBe('COM1');

    // @ts-expect-error
    expect(wideConfig.config.serialPort.slice(1)).toBe('OM1');
    expect(wideConfig.config.serialPort?.slice(1)).toBe('OM1');

    expect(wideConfig.config.speed).toBeUndefined();
    // @ts-expect-error
    expect(wideConfig.config.speed + 100).toBeNaN();
  });

  test('keeps common properties accessible', () => {
    const wideConfig: Widen<NestedConfig> = NESTED_CONFIG;
    expect(wideConfig.info.name.slice(1)).toBe('LS');
  });
});
