import _ from 'lodash';

import { ensureArray } from './array';

export function changeCollection<T, S extends keyof T>(
  collection: null, key: S, value: T[S], change: ((item: T) => T) | Partial<T>
): null;
export function changeCollection<T, S extends keyof T>(
  collection: T[], key: S, value: T[S], change: ((item: T) => T) | Partial<T>
): T[];
export function changeCollection<T, S extends keyof T>(
  collection: T[] | null, key: S, value: T[S], change: ((item: T) => T) | Partial<T>
): T[] | null;
/**
 * Modifies objects whose keys match passed value using provided function. Returns a new array.
 *
 * @param collection an array to modify
 * @param key name of the key (e.g. 'id')
 * @param value value of the key (e.g. 3)
 * @param change transformation function or partial object to be spread into the matching objects
 */
export function changeCollection<T, S extends keyof T>(
  collection: T[] | null, key: S, value: T[S], change: ((item: T) => T) | Partial<T>
): T[] | null {
  if (!collection) { return collection }
  const transform = typeof change === 'function' ? change : (item: T) => ({ ...item, ...change });
  return collection.map(item => item[key] === value ? transform(item) : item);
}

type Dictionary<T> = Record<string, T>;
type Modifier<T> = ((item: T) => T) | (T extends object ? Partial<T> : never);

export function changeDictionary<T>(
  collection: null,
  key: string,
  change: Modifier<T>,
  defaultValue?: (key: string) => T,
): null;
export function changeDictionary<T>(
  collection: Dictionary<T>,
  key: string,
  change: Modifier<T>,
  defaultValue?: (key: string) => T,
): Dictionary<T>;
export function changeDictionary<T>(
  collection: Dictionary<T> | null,
  key: string,
  change: Modifier<T>,
  defaultValue?: (key: string) => T,
): Dictionary<T> | null;
/**
 * Modifies an object with key in the dictionary using provided function. Returns a new dictionary.
 *
 * @param collection
 * @param key key of the object in the dictionary.
 * @param change transformation function or partial object to be spread into the matching objects
 * @param defaultValue Function to provide a new object if the key is not found in the dictionary or is falsy.
 */
export function changeDictionary<T>(
  collection: Dictionary<T> | null,
  key: string,
  change: Modifier<T>,
  defaultValue?: (key: string) => T,
): Dictionary<T> | null {
  if (!collection) {
    return collection;
  }
  if (!(key in collection) && !defaultValue) {
    return collection;
  }
  const transform = typeof change === 'function' ? change : (item: T) => ({ ...item, ...change });
  const oldValue = defaultValue ? (collection[key] ?? defaultValue(key)) : collection[key];
  return {
    ...collection,
    [key]: transform(oldValue),
  };
}

export function filterDictionary<T>(collection: null, predicate: (value: T, key: string) => boolean): null;
export function filterDictionary<T>(collection: Dictionary<T>, predicate: (value: T, key: string) => boolean): Dictionary<T>;
export function filterDictionary<T>(collection: Dictionary<T> | null, predicate: (value: T, key: string) => boolean): Dictionary<T> | null;
/**
 * Returns a new dictionary without values that do not pass the provided predicate.
 */
export function filterDictionary<T>(
  collection: Dictionary<T> | null,
  predicate: (value: T, key: string) => boolean,
): Dictionary<T> | null {
  if (!collection) {
    return collection;
  }
  const toRemove = Object.keys(collection).filter(key => !predicate(collection[key], key));
  if (toRemove.length === 0) {
    return collection;
  }
  return _.omit(collection, ...toRemove);
}

/**
 * Writes to a list of keys in a dictionary, creating them if they're not there
 * @param collection The collection to mutate
 * @param k A key or list of keys to target
 * @param transform A function that given a key and the old value (undefined if there's no value for the key), returns the new value
 * @returns The mutated collection
 */
export function writeToDictionary<Val, K extends string | number>(
  collection: Record<K, Val>,
  k: K | K[],
  transform: (key: K, oldValue?: Val) => Val,
): Record<K, Val> {
  return {
    ...collection,
    ...Object.fromEntries(ensureArray(k).map(key => {
      const oldValue = collection[key];
      const newValue = transform(key, oldValue);
      return [key, newValue];
    }))
  };
}
