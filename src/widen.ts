/* eslint-disable @typescript-eslint/no-explicit-any,@typescript-eslint/no-unused-vars */

// https://stackoverflow.com/questions/60114191/typescript-union-type-to-deep-intersection-of-optional-values-difficulty-leve

type AllKeys<T> = T extends any ? keyof T : never;
type OptionalKeys<T> = T extends any ?
    { [K in keyof T]-?: unknown extends Pick<T, K> ? K : never }[keyof T] : never;
type Idx<T, K extends PropertyKey, D = never> =
    T extends any ? K extends keyof T ? T[K] : D : never;
type PartialKeys<T, K extends keyof T> =
    Omit<T, K> & Partial<Pick<T, K>> extends infer O ? { [P in keyof O]: O[P] } : never;

/** Deep UnionToIntersection that also makes distinct keys partial. */
export type DeepWiden<T> =
    [T] extends [(infer E)[]] ? { [K in keyof T]: DeepWiden<T[K]> } :
    [T] extends [object] ? PartialKeys<
        { [K in AllKeys<T>]: DeepWiden<Idx<T, K>> },
        Exclude<AllKeys<T>, keyof T> | OptionalKeys<T>
    > :
    T;

/** UnionToIntersection that also makes distinct keys partial. */
export type Widen<T> =
    [T] extends [object] ? PartialKeys<
        { [K in AllKeys<T>]: Idx<T, K> },
        Exclude<AllKeys<T>, keyof T> | OptionalKeys<T>
    > :
    T;
