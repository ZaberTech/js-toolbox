import _ from 'lodash';

/**
 * Converts plain JS object transforming all keys using provided function.
 */
export function convert(obj: unknown, keyTransform: (key: string) => string): unknown {
  if (Array.isArray(obj)) {
    return obj.map(item => convert(item, keyTransform));
  } else if (obj && typeof obj === 'object') {
    const newItem: Record<string, unknown> = {};
    for (const key of Object.keys(obj)) {
      newItem[keyTransform(key)] = convert((obj as Record<string, unknown>)[key], keyTransform);
    }
    return newItem;
  } else {
    return obj;
  }
}

/**
 * Converts plain JS object transforming all keys to snake_case.
 */
export const convertForApi = <ToConvert>(obj: ToConvert) => convert(obj, key => _.snakeCase(key));

/**
 * Converts plain JS object transforming all keys to camelCase.
 */
export const convertFromApi = <Result>(obj: unknown) => convert(obj, key => _.camelCase(key)) as Result;
