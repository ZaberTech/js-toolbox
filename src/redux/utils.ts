import { AnyAction } from 'redux';

export const actionBuilder = <T>(type: string, payload?: T) => ({ type, payload });
export const getPayload = <T>(action: Record<string, unknown>) => action.payload as T;

export type ActionToReducerMap<Map, State> = Partial<{ [Key in keyof Map]: (state: State, payload: Map[Key]) => State }>;
export const createReducer = <Map, State>(map: ActionToReducerMap<Map, State>, initialState: State) => (
  (state: State = initialState, action: AnyAction) => {
    const reducer = map[action.type as keyof Map];
    if (reducer) {
      return reducer(state, getPayload(action));
    } else {
      return state;
    }
  }
);
