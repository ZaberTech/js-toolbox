import { applyMiddleware, createStore, Middleware } from 'redux';
import createSagaMiddleware, { SagaIterator, SagaMiddleware, Task } from 'redux-saga';
import { race, take } from 'redux-saga/effects';
import { Subject } from 'rxjs';

import { ensureError } from '../errors';

import { rxToEventChannel } from './channel';

let sagaMiddleware: SagaMiddleware;

beforeAll(() => {
  sagaMiddleware = createSagaMiddleware();

  const middlewares: Middleware[] = [
    sagaMiddleware,
  ];

  createStore(
    state => state ?? {},
    {},
    applyMiddleware(...middlewares),
  );
});

describe('rxToEventChannel', () => {
  let observable: Subject<string>;
  let values: string[];
  let error: Error | null;
  let done = false;

  function* saga(): SagaIterator {
    const channel = rxToEventChannel(observable);
    try {
      while (true) {
        const { value } = yield race({
          value: take(channel),
        });
        if (value) {
          values.push(value);
        }
        if (value === 'close') {
          channel.close();
          break;
        }
      }
    } catch (err) {
      error = ensureError(err);
    } finally {
      done = true;
    }
  }

  let task: Task;

  beforeEach(() => {
    observable = new Subject();
    values = [];
    error = null;
    done = false;

    task = sagaMiddleware.run(saga);
  });

  afterEach(() => {
    task.cancel();
  });

  test('completes when observable completes', async () => {
    observable.complete();

    expect(done).toBe(true);
    expect(error).toBeNull();
    expect(values).toHaveLength(0);
  });

  test('propagates values from observable', async () => {
    observable.next('1');
    expect(values).toEqual(['1']);

    observable.next('2');
    expect(values).toEqual(['1', '2']);
  });

  test('propagates error from observable', async () => {
    observable.error(new Error('fail'));

    expect(error).toMatchObject({ message: 'fail' });
  });

  test('closing unsubscribes from observable', async () => {
    observable.next('close');
    expect(observable.observers).toHaveLength(0);
  });
});
