import { END, EventChannel, eventChannel } from 'redux-saga';
import type { Observable  } from 'rxjs';

import { ensureError } from '../errors';

// eslint-disable-next-line @typescript-eslint/no-empty-object-type
export function rxToEventChannel<T extends {} | null>(observable: Observable<T>): EventChannel<T> {
  return eventChannel<T>(observer => {
    const subscription =  observable.subscribe({
      next: item => observer(item),
      complete: () => observer(END),
      error: err => observer(ensureError(err) as unknown as END)
    });
    return () => subscription.unsubscribe();
  });
}
