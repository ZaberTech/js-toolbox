import { applyMiddleware, createStore, Middleware, Store } from 'redux';
import createSagaMiddleware, { SagaIterator, SagaMiddleware } from 'redux-saga';
import { call, cancelled, delay, join } from 'redux-saga/effects';

import { defer, waitTick } from '../test';

import { safeSpawn, takeLatestUniq, takeLeadingUniq } from './effects';
import { Action, handledBySagaErrorSymbol } from './types';

let store: Store;
let sagaMiddleware: SagaMiddleware;

beforeEach(() => {
  sagaMiddleware = createSagaMiddleware({
    onError: (err: Error & { [handledBySagaErrorSymbol]: true }) => {
      if (err[handledBySagaErrorSymbol]) {
        return;
      }
      console.error(err); // eslint-disable-line no-console
    }
  });

  const middlewares: Middleware[] = [
    sagaMiddleware,
  ];

  store = createStore(
    state => state ?? {},
    {},
    applyMiddleware(...middlewares),
  );
});

afterEach(() => {
  store = null!;
  sagaMiddleware = null!;
});

describe('takeLeadingUniq', () => {
  const TEST_VALUE = 15;
  const testFn = jest.fn<Promise<void> | void, [string, number]>().mockResolvedValue();

  type TestAction = Action<{ key: string  }>;
  function createTestAction(key: string): TestAction {
    return { type: 'TEST', payload: { key } };
  }

  function* rootSaga(): SagaIterator {
    yield takeLeadingUniq<TestAction, typeof handler>('*', action => action.payload.key, handler, TEST_VALUE);
  }

  function* handler(arg: number, action: TestAction): SagaIterator {
    yield call(testFn, action.payload.key, arg);
  }

  beforeEach(() => {
    testFn.mockReset();

    sagaMiddleware.run(rootSaga);
  });

  test('calls the handler with argument and action', () => {
    store.dispatch(createTestAction('key1'));
    expect(testFn).toHaveBeenCalledWith('key1', TEST_VALUE);
  });

  test('prevents the handler to be called simultaneously for the same key', async () => {
    const deferred = defer<void>();
    testFn.mockImplementation(() => deferred.promise);

    store.dispatch(createTestAction('key1'));
    store.dispatch(createTestAction('key2'));
    store.dispatch(createTestAction('key1'));
    store.dispatch(createTestAction('key3'));

    expect(testFn).toHaveBeenCalledTimes(3);
    expect(testFn).toHaveBeenNthCalledWith(1, 'key1', TEST_VALUE);
    expect(testFn).toHaveBeenNthCalledWith(2, 'key2', TEST_VALUE);
    expect(testFn).toHaveBeenNthCalledWith(3, 'key3', TEST_VALUE);

    deferred.resolve();
    await waitTick();

    store.dispatch(createTestAction('key1'));

    expect(testFn).toHaveBeenCalledTimes(4);
    expect(testFn).toHaveBeenNthCalledWith(4, 'key1', TEST_VALUE);
  });

  test('works for sync functions too', () => {
    testFn.mockImplementation(() => undefined);

    store.dispatch(createTestAction('key1'));
    store.dispatch(createTestAction('key1'));

    expect(testFn).toHaveBeenCalledTimes(2);
  });
});

describe('takeLatestUniq', () => {
  const testFn = jest.fn<Promise<void> | void, [string, number]>().mockResolvedValue();
  const cancelledFn = jest.fn();
  const doneFn = jest.fn();

  type TestAction = Action<{ key: string; value: number  }>;
  function createTestAction(key: string, value: number): TestAction {
    return { type: 'TEST', payload: { key, value } };
  }

  function* rootSaga(): SagaIterator {
    yield takeLatestUniq<TestAction, typeof handler>('*', action => action.payload.key, handler);
  }

  function* handler({ payload: { key, value } }: TestAction): SagaIterator {
    try {
      yield call(testFn, key, value);
    } finally {
      if (yield cancelled()) {
        cancelledFn(key, value);
      } else {
        doneFn(key, value);
      }
    }
  }

  beforeEach(() => {
    testFn.mockReset();
    cancelledFn.mockReset();
    doneFn.mockReset();

    sagaMiddleware.run(rootSaga);
  });

  test('calls the handler with argument and action', async () => {
    store.dispatch(createTestAction('key1', 42));
    expect(testFn).toHaveBeenCalledWith('key1', 42);
    await waitTick();
    expect(doneFn).toHaveBeenCalledWith('key1', 42);
  });

  test('cancels the previous handler for the same key', async () => {
    const deferred = defer<void>();
    testFn.mockImplementation(() => deferred.promise);

    store.dispatch(createTestAction('key1', 1));
    expect(testFn).toHaveBeenCalledWith('key1', 1);
    expect(cancelledFn).toHaveBeenCalledTimes(0);

    store.dispatch(createTestAction('key2', 2));
    expect(testFn).toHaveBeenCalledWith('key2', 2);
    expect(cancelledFn).toHaveBeenCalledTimes(0);

    store.dispatch(createTestAction('key1', 3));
    expect(testFn).toHaveBeenCalledWith('key1', 3);
    expect(cancelledFn).toHaveBeenCalledTimes(1);
    expect(cancelledFn).toHaveBeenCalledWith('key1', 1);

    store.dispatch(createTestAction('key3', 4));
    expect(testFn).toHaveBeenCalledWith('key3', 4);
    expect(cancelledFn).toHaveBeenCalledTimes(1);

    store.dispatch(createTestAction('key1', 5));
    expect(testFn).toHaveBeenCalledWith('key1', 5);
    expect(cancelledFn).toHaveBeenCalledTimes(2);
    expect(cancelledFn).toHaveBeenCalledWith('key1', 3);

    expect(doneFn).toHaveBeenCalledTimes(0);
    deferred.resolve();
    await waitTick();
    expect(doneFn).toHaveBeenCalledTimes(3);
    expect(doneFn).toHaveBeenCalledWith('key3', 4);
    expect(doneFn).toHaveBeenCalledWith('key2', 2);
    expect(doneFn).toHaveBeenCalledWith('key1', 5);

    store.dispatch(createTestAction('key1', 6));
    expect(testFn).toHaveBeenLastCalledWith('key1', 6);

    await waitTick();
    expect(doneFn).toHaveBeenCalledTimes(4);
  });

  test('works for sync functions too', () => {
    testFn.mockImplementation(() => undefined);

    store.dispatch(createTestAction('key1', 1));
    expect(doneFn).toHaveBeenCalledWith('key1', 1);

    store.dispatch(createTestAction('key1', 2));
    expect(doneFn).toHaveBeenCalledWith('key1', 2);

    expect(testFn).toHaveBeenCalledTimes(2);
  });
});

describe('safeSpawn', () => {
  test('marks thrown exceptions with the symbol', () =>
    sagaMiddleware.run(function* (): SagaIterator {
      const task = yield safeSpawn(function* (): SagaIterator {
        yield delay(0);
        throw new Error('Bad error');
      });

      try {
        yield join(task);
        throw new Error('No error was observed');
      } catch (err) {
        expect((err as Record<symbol, boolean>)[handledBySagaErrorSymbol]).toBe(true);
        expect((err as Error).message).toBe('Bad error');
      }
    }).toPromise()
  );
});
