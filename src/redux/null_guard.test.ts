import { nullGuard } from './null_guard';

describe('nullGuard', () => {
  function sum(a: number, b:number): number {
    if (a === null || b === null) {
      throw new Error('a and b must not be null');
    }
    return a + b;
  }
  const sumGuarded = nullGuard(sum);

  test('returns null without calling the function when one argument is null', () => {
    expect(sumGuarded(1, null)).toBe(null);
    expect(sumGuarded(null, 2)).toBe(null);
  });
  test('returns return value of the original function no argument is null', () => {
    expect(sumGuarded(1, 2)).toBe(3);
  });
});
