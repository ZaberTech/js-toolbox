/* eslint-disable @typescript-eslint/no-explicit-any */
import type { StrictEffect } from 'redux-saga/effects';

import { AnyFunction } from '../types';

export const handledBySagaErrorSymbol = Symbol('handledBySagaError');

export type Action<T> = { type: string; payload: T };
export type EmptyAction = { type: string };
export type AnyAction = { type: string; payload?: unknown };

export type SagaIter<RT = any> = Iterator<StrictEffect | Promise<any> | SagaIter, RT, any>;

/**
 * Use to infer return types of Sagas, Async and ordinary functions.
 */
export type RT<S extends AnyFunction> =
  S extends (...args: any[]) => SagaIter<infer RType> ? RType :
  S extends (...args: any[]) => Promise<infer RType> ? RType :
  S extends (...args: any[]) => infer RType ? RType :
  never;
