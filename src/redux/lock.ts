import { Mutex, MutexInterface } from 'async-mutex';
import { CANCEL } from 'redux-saga';

interface Unlock {
  unlock: MutexInterface.Releaser;
}

// eslint-disable-next-line @typescript-eslint/consistent-indexed-object-style
interface SagaCancellablePromise extends Promise<Unlock> {
  [cancel: string]: () => void;
}

export class SagaLock {
  private readonly _lock = new Mutex();

  constructor() {
    this.lock = this.lock.bind(this);
  }

  public lock(): Promise<Unlock> {
    const lockPromise = this._lock.acquire().then(unlock => ({ unlock }));

    // this will unlock the lock if the call effect is cancelled
    (lockPromise as SagaCancellablePromise)[CANCEL] = () => lockPromise.then(({ unlock }) => unlock());

    return lockPromise;
  }
}
