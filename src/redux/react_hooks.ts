import { useMemo } from 'react';
import { useDispatch } from 'react-redux';
import { ActionCreatorsMapObject, bindActionCreators } from 'redux';

/**
 * A react hook to get the actions object that can be used to run actions on the current redux store
 *
 * @param actionCreator action creator for the actions we want to use
 * @param deps a list of state variables that will trigger the returned actions object to be
 * recreated if the values of the listed variables changes
 */
export function useActions<A, T extends ActionCreatorsMapObject<A>>(actionCreator: T, deps?: unknown[]): T {
  const dispatch = useDispatch();
  return useMemo(
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    () => bindActionCreators(actionCreator, dispatch),
    deps ? [dispatch, ...deps] : [dispatch]
  );
}
