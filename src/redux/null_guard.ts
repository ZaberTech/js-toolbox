import { Nullable } from '../types';

export function nullGuard<R, A1>(guardedFunction: (a1: A1) => R): (a1: Nullable<A1>) => R | null;
export function nullGuard<R, A1, A2>(guardedFunction: (a1: A1, a2: A2) => R):
  (a1: Nullable<A1>, a2: Nullable<A2>) => R | null;
export function nullGuard<R, A1, A2, A3>(guardedFunction: (a1: A1, a2: A2, a3: A3) => R):
  (a1: Nullable<A1>, a2: Nullable<A2>, a3: Nullable<A3>) => R | null;
export function nullGuard<R, A1, A2, A3, A4>(guardedFunction: (a1: A1, a2: A2, a3: A3, a4: A4) => R):
  (a1: Nullable<A1>, a2: Nullable<A2>, a3: Nullable<A3>, a4: Nullable<A4>) => R | null;
export function nullGuard<R, A1, A2, A3, A4, A5>(guardedFunction: (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5) => R):
  (a1: Nullable<A1>, a2: Nullable<A2>, a3: Nullable<A3>, a4: Nullable<A4>, a5: Nullable<A5>) => R | null;

/* eslint-disable prefer-rest-params, @typescript-eslint/no-explicit-any */
export function nullGuard<T extends(...args: any[]) => any>(guardedFunction: T): any {
  return function(this: unknown): unknown {
    for (const argument of arguments) {
      if (argument === null) {
        return null;
      }
    }
    return guardedFunction.apply(this, arguments as any); // eslint-disable-line
  } as T;
}
