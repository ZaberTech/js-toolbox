import { take, call, fork, ForkEffect, ActionPattern, HelperWorkerParameters, spawn } from 'redux-saga/effects';
import { SagaIterator, Task } from 'redux-saga';
import { Action as ReduxAction } from 'redux';

import { AnyFunction } from '../types';

import { handledBySagaErrorSymbol } from './types';

export function takeLeadingUniq<TAction extends ReduxAction>(
    pattern: ActionPattern<TAction>,
    getKey: (action: TAction) => string,
    worker: (action: TAction) => unknown,
  ): ForkEffect;

export function takeLeadingUniq<TAction extends ReduxAction, Fn extends AnyFunction>(
  pattern: ActionPattern<TAction>,
  getKey: (action: TAction) => string,
  worker: Fn,
  ...args: HelperWorkerParameters<TAction, Fn>
): ForkEffect;

export function takeLeadingUniq<TAction extends ReduxAction, Fn extends AnyFunction>(
  pattern: ActionPattern<TAction>,
  getKey: (action: TAction) => string,
  worker: Fn,
  ...args: HelperWorkerParameters<TAction, Fn>
): ForkEffect {
  return takeUniq('leading', pattern, getKey, worker, ...args);
}

export function takeLatestUniq<TAction extends ReduxAction>(
    pattern: ActionPattern<TAction>,
    getKey: (action: TAction) => string,
    worker: (action: TAction) => unknown,
  ): ForkEffect;

export function takeLatestUniq<TAction extends ReduxAction, Fn extends AnyFunction>(
  pattern: ActionPattern<TAction>,
  getKey: (action: TAction) => string,
  worker: Fn,
  ...args: HelperWorkerParameters<TAction, Fn>
): ForkEffect;

export function takeLatestUniq<TAction extends ReduxAction, Fn extends AnyFunction>(
  pattern: ActionPattern<TAction>,
  getKey: (action: TAction) => string,
  worker: Fn,
  ...args: HelperWorkerParameters<TAction, Fn>
): ForkEffect {
  return takeUniq('latest', pattern, getKey, worker, ...args);
}

export function takeUniq<TAction extends ReduxAction, Fn extends AnyFunction>(
  mode: 'leading' | 'latest',
  pattern: ActionPattern<TAction>,
  getKey: (action: TAction) => string,
  worker: Fn,
  ...args: HelperWorkerParameters<TAction, Fn>
): ForkEffect {
  return fork(function* (): SagaIterator {
    const tasks = new Map<string, Task>();

    while (true) {
      const action = yield take(pattern);
      // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
      const key = getKey(action);
      if (typeof key !== 'string' || key === '') {
        throw new Error(`Invalid key returned: ${key}`);
      }

      const oldTask = tasks.get(key);
      if (oldTask?.isRunning()) {
        switch (mode) {
          case 'leading':
            continue;
          case 'latest':
            tasks.delete(key);
            oldTask.cancel();
            break;
        }
      }

      let task: Task | null = null;

      task = (yield fork(function* (): SagaIterator {
        yield call<any>(worker, ...args, action); // eslint-disable-line @typescript-eslint/no-explicit-any

        if (tasks.get(key) === task) {
          tasks.delete(key);
        }
      })) as Task;

      if (task.isRunning()) {
        tasks.set(key, task);
      }
    }
  });
}

/**
 * Works like spawn but marks thrown exceptions so that they can recognized as handled by the root error handler.
 * Make sure to join the returned task to observe the exception.
 */
export function safeSpawn<Fn extends AnyFunction>(
  worker: Fn,
  ...args: Parameters<Fn>
): ReturnType<typeof spawn> {
  return spawn(function* (): SagaIterator {
    try {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-return
      return yield call(worker, ...args);
    } catch (err) {
      (err as Record<symbol, boolean>)[handledBySagaErrorSymbol] = true;
      throw err;
    }
  });
}
