import { changeCollection, changeDictionary, filterDictionary, writeToDictionary } from './mutation';

describe('changeCollection', () => {
  test('changes the item with specified id with modifier function', () => {
    const connections = [{ id: 1, x: 1 }, { id: 2, x: 2 }, { id: 3, x: 3 }];
    const connectionsNew = changeCollection(connections, 'id', 2, item => ({ ...item, x: 4 }));
    expect(connections !== connectionsNew).toBeTruthy();
    expect(connectionsNew).toEqual([{ id: 1, x: 1 }, { id: 2, x: 4 }, { id: 3, x: 3 }]);
  });
  test('changes the item with specified id with with partial item data', () => {
    const connections = [{ id: 1, x: 1 }, { id: 2, x: 2 }, { id: 3, x: 3 }];
    const connectionsNew = changeCollection(connections, 'id', 2, { x: 4 });
    expect(connections !== connectionsNew).toBeTruthy();
    expect(connectionsNew).toEqual([{ id: 1, x: 1 }, { id: 2, x: 4 }, { id: 3, x: 3 }]);
  });
  test('returns null on null collection', () => {
    const collection: { id: number }[] | null = null;
    expect(changeCollection(collection, 'id', 2, { id: 3 })).toBeNull();
  });
});

describe('changeDictionary', () => {
  test('changes the item with specified id with modifier function', () => {
    const connections = {
      a: { id: 'a', x: 1 },
      b: { id: 'b', x: 2 },
      c: { id: 'c', x: 3 },
    };
    const connectionsNew = changeDictionary(connections, 'b', item => ({ ...item, x: 4 }));
    expect(connections !== connectionsNew).toBeTruthy();
    expect(connectionsNew).toEqual({
      a: { id: 'a', x: 1 },
      b: { id: 'b', x: 4 },
      c: { id: 'c', x: 3 },
    });
  });
  test('changes the item with specified id with with partial item data', () => {
    const connections = {
      a: { id: 'a', x: 1 },
      b: { id: 'b', x: 2 },
      c: { id: 'c', x: 3 },
    };
    const connectionsNew = changeDictionary(connections, 'b', { x: 4 });
    expect(connections !== connectionsNew).toBeTruthy();
    expect(connectionsNew).toEqual({
      a: { id: 'a', x: 1 },
      b: { id: 'b', x: 4 },
      c: { id: 'c', x: 3 },
    });
  });
  test('adds item with default value if the item is not present', () => {
    const connections = {
      a: { id: 'a', x: 1 },
      b: { id: 'b', x: 2 },
    };
    const connectionsNew = changeDictionary(connections, 'c', { x: 4 }, id => ({ id, x: 0, y: 1 }));
    expect(connections !== connectionsNew).toBeTruthy();
    expect(connectionsNew).toEqual({
      a: { id: 'a', x: 1 },
      b: { id: 'b', x: 2 },
      c: { id: 'c', x: 4, y: 1 },
    });
  });
  test('returns original dictionary if there is nothing to change', () => {
    const connections = {
      a: { id: 'a', x: 1 },
    };
    const newConnections = changeDictionary(connections, 'b', { x: 2 });
    expect(newConnections).toBe(connections);
  });
  test('returns null on null dictionary', () => {
    expect(changeDictionary<object>(null, 'a', {})).toBeNull();
  });
  test('can handle dictionary with nullable values', () => {
    let collection: Record<string, number | undefined> = { a: 1, b: 2 };

    collection = changeDictionary<number | undefined>(collection, 'b', () => undefined);
    expect(collection).toEqual({ a: 1, b: undefined });

    collection = changeDictionary<number | undefined>(collection, 'b', () => 3);
    expect(collection).toEqual({ a: 1, b: 3 });
  });
});

describe('filterDictionary', () => {
  test('filters dictionary and returns new object', () => {
    const dictionary = { a: 1, b: 2, c: 3, d: 4 };
    const newDictionary = filterDictionary(dictionary, (value, key) => value <= 1 || key === 'd');
    expect(newDictionary).toMatchObject({ a: 1, d: 4 });
    expect(newDictionary).not.toBe(dictionary);
  });
  test('returns null for null', () => {
    expect(filterDictionary(null, () => true)).toBeNull();
  });
  test('does not modify collection if nothing is removed', () => {
    const dictionary = { a: 1 };
    expect(filterDictionary(dictionary, () => true)).toBe(dictionary);
  });
});

describe('writeToDictionary', () => {
  const TEST_DICTIONARY = { a: 1, b: 2, c: 3 } as Record<'a' | 'b' | 'c' | 'd', number>;
  test('updates old value', () => {
    const newDictionary = writeToDictionary(TEST_DICTIONARY, ['a', 'b'],
      (key, oldValue) => (oldValue ?? Number.NaN) + key.charCodeAt(0));
    expect(newDictionary).toEqual({ a: 98, b: 100, c: 3 });
  });
  test('adds to dictionary', () => {
    const newDictionary = writeToDictionary(TEST_DICTIONARY, 'd', () => 4);
    expect(newDictionary).toEqual({ a: 1, b: 2, c: 3, d: 4 });
  });
});
