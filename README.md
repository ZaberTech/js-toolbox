# Zaber Toolbox

Library containing variety of useful functions that we use in our projects. These functions do not contain any significant business logic.

* The root contains miscellaneous Js functions and Typescript utilities.
* `redux` folder contains redux utilities and redux-saga effects.
* `test` folder contains test-related utility functions.

## Installation

The package can be installed from the internal gitlab package registry (installed as `@zaber/toolbox`)

```
echo @software-public:registry=https://gitlab.izaber.com/api/v4/packages/npm/ >> .npmrc
npm i @zaber/toolbox@npm:@software-public/toolbox
```
